﻿using DynamicData;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using Woodvillage.Networking.Models;

namespace Woodvillage.Networking.Services
{
    /// <summary>
    /// Singleton to allow for searching connected machines in a defined network setup.
    /// </summary>
    public sealed class DiscoveryService
    {
        private static readonly Lazy<DiscoveryService> lazy
            = new Lazy<DiscoveryService>(() => new DiscoveryService());
        /// <summary>
        /// Singleton instance
        /// </summary>
        public static DiscoveryService Instance { get { return lazy.Value; } }

        private SourceCache<NetworkDescription, string> _knownNetworks
            = new SourceCache<NetworkDescription, string>(n => n.Name);
        /// <summary>
        /// All changes to available networks will be delivered as an obserable stream.
        /// </summary>
        public IObservable<IChangeSet<NetworkDescription, string>> KnownNetworks => _knownNetworks.Connect();

        private SourceCache<NetworkInterface, string> _availableInterfaces
            = new SourceCache<NetworkInterface, string>(i => i.Name);
        /// <summary>
        /// Create observable changeset with the current network interface configurations.
        /// </summary>
        public IObservable<IChangeSet<NetworkInterface, string>> AvailableInterfaces => _availableInterfaces.Connect();

        private SourceCache<NetworkDescription, string> _availableNetworks
            = new SourceCache<NetworkDescription, string>(i => i.Name);

        public IObservable<IChangeSet<NetworkDescription, string>> AvailableNetworks => _availableNetworks.Connect();
        /// <summary>
        /// Create observable changeset with the reachable networks.
        /// </summary>
        private CompositeDisposable _disposables = new CompositeDisposable();

        /// <summary>
        /// Refresh all known networks.
        /// </summary>
        /// <returns>List of currently found networks.</returns>
        public IEnumerable<NetworkDescription> ReadKnownNetworks()
        {
            try
            {
                if (File.Exists(@"known_networks.json"))
                {
                    var json = File.ReadAllText(@"known_networks.json");
                    var knownNetworksFromJson = JsonConvert.DeserializeObject<IEnumerable<NetworkDescription>>(json);
                    _knownNetworks.Clear();
                    _knownNetworks.AddOrUpdate(knownNetworksFromJson);
                    return knownNetworksFromJson;
                }
                else
                {
                    File.WriteAllText(@"known_networks.json", "[]");
                }
            }
            catch { }
            return new List<NetworkDescription>();
        }

        private DiscoveryService()
        {
            ReadKnownNetworks();

            _disposables.Add(LocalInterfaces.GetActiveLocalNetworkInterfaces()
                .Subscribe(list =>
                {
                    _availableInterfaces.Remove(
                        _availableInterfaces.Keys.Except(list.Select(t => t.Name)).ToList());
                    _availableInterfaces.AddOrUpdate(list);
                }));

            _disposables.Add(LocalInterfaces.GetActiveLocalIpAddresses()
                .Select(addresses =>
                {
                    var networks = new List<NetworkDescription>();
                    foreach (var address in addresses)
                    {
                        var matchingNetworks = _knownNetworks.Items
                            .Where(network => network.Contains(address));
                        networks.AddRange(matchingNetworks.Count() != 0 ? matchingNetworks :
                            new List<NetworkDescription> { new NetworkDescription { Subnets = { address } } });
                    }
                    return networks;
                })
                .Subscribe(list =>
                {
                    _availableNetworks.Remove(
                            _availableNetworks.Keys.Except(list.Select(t => t.Name)).ToList());
                    _availableNetworks.AddOrUpdate(list);
                }));
        }
    }
}
