﻿using DynamicData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using System.Runtime.Serialization;
using System.Text;
using Woodvillage.Core.Viewmodels;

namespace Woodvillage.Networking.Models
{
    [DataContract]
    public class NetworkDescription : ViewmodelBase
    {
        [DataMember]
        public string Name { get; set; } = "Unknown";

        [DataMember]
        public ObservableCollection<string> Subnets { get; set; }
            = new ObservableCollection<string>();

        [DataMember]
        public ObservableCollection<HostDescription> KnownHosts { get; set; }
            = new ObservableCollection<HostDescription>();

        [DataMember]
        public int Priority { get; set; } = 0;

        [DataMember]
        private bool _showUnknownHosts = false;
        public bool ShowUnknownHosts
        {
            get => _showUnknownHosts;
            set
            {
                SetField(ref _showUnknownHosts, value);
                _availableHostsCache.Refresh();
            }
        }

        private SourceCache<HostDescription, string> _availableHostsCache
            = new SourceCache<HostDescription, string>(h => h.IpAddress);
        private readonly ReadOnlyObservableCollection<HostDescription> _availableHosts;
        public ReadOnlyObservableCollection<HostDescription> AvailableHosts => _availableHosts;

        public NetworkDescription()
        {
            _disposables.Add(_availableHostsCache.Connect()
                .Sort(new IpAddressComparer())
                .Filter(h => KnownHosts.Contains(h) || ShowUnknownHosts)
                .Bind(out _availableHosts)
                .DisposeMany()
                .Subscribe());
        }

        public bool Contains(string ipAddress)
        {
            return this.Subnets.Contains(ipAddress, new IsInSubnetComparer());
        }

        public class IsInSubnetComparer : IEqualityComparer<string>
        {
            public bool Equals(string x, string y) =>
                x.Substring(0, x.LastIndexOf(".")) == y.Substring(0, y.LastIndexOf("."));

            public int GetHashCode(string obj) => obj.GetHashCode();
        }

        private class IpAddressComparer : IComparer<HostDescription>
        {
            public int Compare(HostDescription a, HostDescription b)
            {
                return Enumerable.Zip(a.IpAddress.Split('.'), b.IpAddress.Split('.'),
                                     (x, y) => int.Parse(x).CompareTo(int.Parse(y)))
                                 .FirstOrDefault(i => i != 0);
            }
        }

        public void ScanSubnets()
        {
            var localAddresses = LocalInterfaces.GetActiveLocalIpAddresses().Take(1).Wait();
            _availableHostsCache.Clear();
            Observable.ToObservable(Subnets)
                .SelectMany(subnet => ExtendedPing.ScanSubnet(subnet))
                .Subscribe(reply =>
                {
                    var replyAddress = reply.Address.ToString();
                    if (localAddresses.Contains(replyAddress)) return;

                    if (this.Contains(replyAddress))
                    {
                        HostDescription newHost = this.KnownHosts.ToList()
                            .Find(h => h.IpAddress == replyAddress);
                        if (newHost == null)
                        {
                            newHost = new HostDescription
                            {
                                IpAddress = replyAddress
                            };
                        }
                        newHost.ActivatePing();
                        _availableHostsCache.AddOrUpdate(newHost);
                    }
                });
        }
    }
}
