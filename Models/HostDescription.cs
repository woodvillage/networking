﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Woodvillage.Core.Viewmodels;

namespace Woodvillage.Networking.Models
{
    [DataContract]
    public class HostDescription : ViewmodelBase
    {
        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public string IpAddress { get; set; }

        [DataMember]
        public bool Mandatory { get; set; } = false;

        private bool _pingResult = false;
        public bool PingResult
        {
            get { return _pingResult; }
            set { SetField(ref _pingResult, value); }
        }

        public void ActivatePing()
        {
            _disposables.Add(ExtendedPing.CreateContinous(IpAddress, TimeSpan.FromSeconds(5))
                .Subscribe(reply => PingResult = reply.Status == System.Net.NetworkInformation.IPStatus.Success));
        }
    }
}
