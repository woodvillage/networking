﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;

namespace Woodvillage.Networking
{
    /// <summary>
    /// Static class to retrieve local network interface configuration as an observable
    /// with updates running through the obserable, when configuration changes.
    /// </summary>
    public static class LocalInterfaces
    {
        private static IObservable<IEnumerable<NetworkInterface>> _interfaces =
                Observable
                    .FromEventPattern<EventArgs>(typeof(NetworkChange), "NetworkAddressChanged")
                    .Throttle(TimeSpan.FromSeconds(2))
                    .StartWith(new EventPattern<EventArgs>(null, new EventArgs()))
                    .Select(evt => NetworkInterface.GetAllNetworkInterfaces()
                                    .Where(nic =>
                                        nic.OperationalStatus == OperationalStatus.Up &&
                                        (nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet ||
                                        nic.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 ||
                                        (int)nic.NetworkInterfaceType == 53)));

        /// <summary>
        /// Returns a list of the present network interfaces
        /// </summary>
        /// <returns>Observable of list of network interfaces</returns>
        public static IObservable<IEnumerable<NetworkInterface>> GetActiveLocalNetworkInterfaces()
        {
            return _interfaces;
        }

        /// <summary>
        /// Returns a list of IP addresses associated with the active network interfaces
        /// </summary>
        /// <returns>Observable of list of addresses</returns>
        public static IObservable<IEnumerable<string>> GetActiveLocalIpAddresses()
        {
            return _interfaces
                .Select(list =>
                {
                    var addresses = new List<string>();
                    foreach (var nic in list)
                    {
                        foreach (var uca in nic.GetIPProperties().UnicastAddresses)
                        {
                            if (uca.Address.AddressFamily == AddressFamily.InterNetwork)
                            {
                                addresses.Add(uca.Address.ToString());
                            }
                        }
                    }
                    return addresses;
                });
        }
    }
}
