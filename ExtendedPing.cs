﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Reactive.Linq;
using System.Text;

namespace Woodvillage.Networking
{
    /// <summary>
    /// Static class to generate observables for continous pings or single time mass pings.
    /// </summary>
    public static class ExtendedPing
    {
        /// <summary>
        /// Create an observable with continously pinging a host and return the results in 
        /// an observable stream.
        /// </summary>
        /// <param name="ipaddress">Host address</param>
        /// <param name="interval">Interval for pings</param>
        /// <returns>Observable of ping results</returns>
        public static IObservable<PingReply> CreateContinous
            (string ipaddress, TimeSpan interval)
            /**
                * - Create Ping() instance as a resource
                * - Create an interval and perform a ping each interval
                * - Merge with an observable created from the Ping.PingCompleted event
                */
            => Observable.Using(() => new Ping(),
                (Ping ping) =>
                    Observable.FromEventPattern<PingCompletedEventArgs>(ping, "PingCompleted")
                        .Select(result => result.EventArgs.Reply)
                        .Where(result => result != null)
                        .WithLatestFrom(Observable.Interval(interval).StartWith(0)
                            .Do(i =>
                            {
                                try
                                {
                                    ping.SendAsync(ipaddress, null);
                                }
                                catch (Exception) { }
                            }),
                            (a, b) => a));

        /// <summary>
        /// Scan a subnet once and return the results as an observable stream.
        /// </summary>
        /// <param name="subnet">Address of the subnet (e.g. 192.168.4.0)</param>
        /// <returns>Observable stream with the ping results.</returns>
        public static IObservable<PingReply> ScanSubnet(string subnet)
            => Observable.Range(1, 254).Select(i =>
            {
                var ping = new Ping();
                // todo make sure the parameter is good
                var snPrefix = subnet.Substring(0, subnet.LastIndexOf("."));
                var result = Observable.FromEventPattern<PingCompletedEventArgs>(ping, "PingCompleted").Take(1);
                ping.SendAsync(String.Format("{0}.{1}", snPrefix, i), null);
                return result;
            })
            .Merge()
            .Select(evt => evt.EventArgs.Reply)
            .Where(reply => reply.Status == IPStatus.Success);
    }
}
